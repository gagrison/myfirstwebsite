/*******************************

     Header

*******************************/

function setActiveClass(scrollPosition, navElPosition, nav){ /* Set active class on header navigation based on scroll position */

	if (scrollPosition < (navElPosition.aboutus-140)) {
        nav[0].classList.add('active-header');
    }
    else if (scrollPosition >= (navElPosition.aboutus-140) && scrollPosition < (navElPosition.contact-225)) {
        nav[1].classList.add('active-header');
    }
    else if (scrollPosition >= (navElPosition.contact-225)) {
        nav[3].classList.add('active-header');
    }
}

function removeActiveClass(x,y){ /* Remove active 'y' class on 'x' elements */

	for(var i=0; i<x.length;i++){

		x[i].classList.remove(y);
	}
}

var nav = document.getElementsByTagName('nav')[0].getElementsByTagName('a'),
    hash,
    timeout;

var navElPosition = {
    home: 0,
    aboutus: document.getElementById('aboutus').offsetTop,
    contact: document.getElementsByClassName('right-frame')[0].offsetTop - 16
}

for(var i=0; i<nav.length; i++){

	nav[i].onclick = function(e){ /* Change scroll position and active class on click */

		hash = e.target.hash;
		if(!hash) return;
		e.preventDefault();
		removeActiveClass(nav,'active-header');
		this.classList.add('active-header');
        window.scrollTo(0, navElPosition[hash.slice(1, hash.length)]);
	}
}

setActiveClass(window.scrollY, navElPosition, nav);

window.addEventListener('scroll',function(){ /* Change active class on scroll */

    clearTimeout(timeout);
    timeout = setTimeout(function(){

    	removeActiveClass(nav,'active-header');
        setActiveClass(window.scrollY,navElPosition,nav);

    }, 250)
})	

/*******************************

     Log in button

*******************************/

var login = document.getElementsByClassName('login')[0],
    formDiv = document.getElementsByClassName('formDiv')[0],
    backDiv = document.getElementsByClassName('backDiv')[0],
    leave = [backDiv,formDiv.getElementsByTagName('span')[0]]; /* backDiv and X button */

login.onclick = function(){

    this.classList.add('login-active');
    backDiv.classList.add('visible');
    formDiv.classList.add('visible2');
}

leave.forEach(function(key){

    key.onclick = function(){

    login.classList.remove('login-active');   
    backDiv.classList.remove('visible');
    formDiv.classList.remove('visible2');
    }
})

/*******************************

     Read more link

*******************************/

var readMoreButton = document.getElementById('home').getElementsByTagName('a')[0];

readMoreButton.onclick = function(e){

    e.preventDefault();
    window.scrollTo(0,document.getElementById('readmore').offsetTop);
}

/*******************************

     Get the app button

*******************************/

var getButtons = document.getElementsByClassName('get');

Object.keys(getButtons).forEach(function(key){

    getButtons[key].onclick = function(){

        getButtons[key].style.color = '#ff2626'; 
    }

    getButtons[key].onmouseover = function(){

        getButtons[key].style.color= '#ffffff';
        getButtons[key].style.backgroundColor= 'transparent';
    }

    getButtons[key].onmouseout = function(){

        getButtons[key].style.color= '#3A7BD5';
        getButtons[key].style.backgroundColor= '#ffffff';
    }
})

/*******************************

     First Changer

*******************************/

function opacityAnimation(div){

    var j,
        z=0;

    div.style.opacity = 0;

    j = setInterval(function(){

        if(z>=1){

            clearInterval(j);
        }

        div.style.opacity = z;
        z += 0.05;

    },50)

}

function display(div,num){ /* Show clicked div + opacityAnimation, hide other divs */

    for(var i=0;i<div.length;i++){

        if(i==num){

            div[i].style.display = "block";
            opacityAnimation(div[i]);
    }

    else {

        div[i].style.display = "none";
    }
    }
}

var index = {

    Platforms: 0,
    Devices: 1,
    'The app': 0,
    'The idea': 1,
    'The process': 2,
    News: 0,
    'About us': 1,
    'Our team': 2

};

var div1 = document.getElementById('apDev').getElementsByTagName('div'),
    buttons1 = document.getElementsByClassName('second')[0].getElementsByTagName('button');

for(i=0; i<buttons1.length; i++){

    buttons1[i].onclick = function(){

   if(div1[index[this.innerHTML]].style.display != 'block'){ /* if div[index] is not block */

            display(div1,index[this.innerHTML]); 
            removeActiveClass(buttons1,'active-blue');
            this.classList.add('active-blue');
        }
    }
}

/*******************************

     Second Changer

*******************************/

var div2 = document.getElementsByClassName('third-content')[0].getElementsByTagName('div'),
    buttons2 = document.getElementById('readmore').getElementsByTagName('button');

for(i=0; i<buttons2.length; i++){

    buttons2[i].onclick = function(){

        if(div2[index[this.innerHTML]].style.display != 'block'){ 

            display(div2,index[this.innerHTML]); 
            removeActiveClass(buttons2,'active-white');
            this.classList.add('active-white');
        }     
    }
}

/*******************************

     Third Changer

*******************************/

var div3 = document.getElementById('fourth-content').getElementsByTagName('div'),
    buttons3 = document.getElementById('aboutus').getElementsByTagName('button');

for(i=0; i<buttons3.length;i++){

    buttons3[i].onclick = function(){

       if(div3[index[this.innerHTML]].style.display != 'block'){

            display(div3,index[this.innerHTML]); 
            removeActiveClass(buttons3,'active-blue');
            this.classList.add('active-blue');
        }
    }
}

/*******************************

     Subscribe Form

*******************************/

function validateSubscribe(sub){

    var error = '',
        specialCharPosition = sub.value.indexOf('@');
        
    sub.style.borderColor = '#f00';
    
    if(sub.value.trim().length===0){
        error = "Empty subscribe field"; 
    }
    else if(specialCharPosition===-1){
        error = 'Missing "@" character';
    }
    else if(specialCharPosition===0 || specialCharPosition===sub.value.length-1){
        error = 'Invalid email';
    }
   
   else{
       
       error = '';
       sub.style.borderColor = '#3A7BD5';
   }
    return error;
}

var subForma = document.subForm,
    subsribe = document.getElementById('subscribe');

subForma.onsubmit = function(e){

    e.preventDefault();
    var reason = validateSubscribe(subscribe);

    if(reason.length>0){

        alert(reason);
    }

    else{

        alert('Email sent');
        subscribe.value = ''; /* Reset */
    }

}

/*******************************

     Contact button

*******************************/

var contactButton = document.getElementById('contact-button'),
    arrow = contactButton.getElementsByTagName('span')[0];

contactButton.onmouseover = function(){ /* Rotate arrow */

    arrow.style.transform = "scale(0.29) rotate(180deg)";
}

contactButton.onmouseout = function(){

    arrow.style.transform = "scale(0.29) rotate(0)";
}

/*******************************

     Contact form

*******************************/

var BORDERCOLOR = {

    error: '#f00',
    none: 'transparent',
    focus: '#ffffff'
}

function validateSubject(subject){

    var error = '';
     
    if(subject.value.trim().length===0){ 
        
       subject.style.borderColor = BORDERCOLOR.error;
       error = "Empty subject field";   
       
    }
    else {
        
    error = '';
    subject.style.borderColor = BORDERCOLOR.none;
    
    }
    
    return error;
}

function validateEmail(email){

    var error = '',
        specialCharPosition = email.value.indexOf('@');
        
    email.style.borderColor = BORDERCOLOR.error;
    
    if(email.value.trim().length===0){
        error = "Empty email field"; 
    }
    else if(specialCharPosition===-1){
        error = 'Missing "@" character';
    }
    else if(specialCharPosition===0 || specialCharPosition=== email.value.length-1){
        error = 'Invalid email';
    }
   
   else{
       
       error = '';
       email.style.borderColor = BORDERCOLOR.none;
   }
    return error;
}

function validateMessage(message){

    var error = '';
     
    if(message.value.trim().length===0){ 
        
       message.style.borderColor = BORDERCOLOR.error;
       error = "Empty message field";   
       
    }
    else {
        
    error = '';
    message.style.borderColor = BORDERCOLOR.none;
    
    }
    
    return error;
}

var forma = document.formLast;

forma.onsubmit = function(e){

    e.preventDefault();
    var reason = '';
    reason += validateEmail(forma.email) + '\n';
    reason += validateSubject(forma.subject) + '\n';
    reason += validateMessage(forma.message);
    
    if(reason.length > 2){
        alert(reason);
        return;
    }

    else{
        alert("Form sent");
        forma.email.value = ''; /* Reset */
        forma.subject.value = '';
        forma.message.value = '';
    }
} 

Object.keys(forma).forEach(function(key){

if(key!=3){ /* If input is not index-3(button) */

    forma[key].onfocus = function(){

        forma[key].style.borderColor = BORDERCOLOR.focus;
    }

    forma[key].onblur = function(){

        forma[key].style.borderColor = BORDERCOLOR.none;
    }
}
})

/*******************************

     Log in Form

*******************************/

function validatePassword(pass){

    var error = '',
        myPassword = 'Mostar123';

    if(pass.value===myPassword){

        error = '';
        pass.style.borderColor = BORDERCOLOR.none;
    }

    else if(pass.value.length===0){

        error="Empty password field";
        pass.style.borderColor = BORDERCOLOR.error;
    }

    else{

        error = 'Incorrect password';
        pass.style.borderColor = BORDERCOLOR.error;
    }
    return error;
}

var logInForm = document.logInForm;

logInForm.onsubmit = function(e){

    e.preventDefault();
    var reason = '';
    reason += validateEmail(logInForm.logInEmail) + '\n';
    reason += validatePassword(logInForm.logInPass);
    
    if(reason.length > 1){
        alert(reason);
        return;
    }
    else{
        alert("Correct");
        logInForm.logInEmail.value = ''; /* Reset */
        logInForm.logInPass.value = '';
    }
}

/*******************************

     Google map

*******************************/

function initialize() {
    var mapProp = {
        center: new google.maps.LatLng(43.352884, 17.793619),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };

    var image = {
        url: 'css/images/icon-map.png',
        size: new google.maps.Size(64, 64)
    };

    var map = new google.maps.Map(document.getElementById("left"), mapProp);

    var marker = new google.maps.Marker({
        map: map,
        position: mapProp.center,
        animation: google.maps.Animation.DROP,
        title: "NSoft, Bleiburških žrtava, Mostar",
        icon: image,
        scrollwheel: mapProp.scrollwheel
    });

}

google.maps.event.addDomListener(window, 'load', initialize); 